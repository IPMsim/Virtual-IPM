from argparse import ArgumentParser
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from virtual_ipm.auxiliaries import read_csv_output_file, run_simulation


parser = ArgumentParser()
parser.add_argument('output', type=Path, help='The output CSV containing the test data '
                                              'or an XML configuration file which will be used to produce that data')
parser.add_argument('reference', type=Path, help='Either CSV file containing reference data or NPY file containing '
                                                 'multiple profiles')
parser.add_argument('--bin-edges', type=Path, help='NPY file containing bin edges for producing histograms')
parser.add_argument('--bins', type=int, help='Number of bins to use for producing histograms '
                                             '(ignored if --bin-edges is specified); '
                                             'must be specified together with --edges')
parser.add_argument('--edges', type=float, nargs=2, help='The x-edges (i.e. min/max x-values) for producing histograms '
                                                         '(ignored if --bin-edges is specified); '
                                                         'must be specified together with --bins')
parser.add_argument('--only-detected', action='store_true', help='Whether to use only DETECTED particles for the plots')
parser.add_argument('--plot-grid', action='store_true', help='Whether to show a grid on the plot')
parser.add_argument('--log-scale', action='store_true', help='Whether to use a loc scale for the y-axis of the plot')
parser.add_argument('--save', type=Path, help='File path for saving the output from a simulation run for XML configuration')
parser.add_argument('--stdout', action='store_true', help='Show simulation output when running from an XML configuration file')


# noinspection PyShadowingNames
def plot_profiles(out, ref, *, x_pos):
    fig, ax = plt.subplots()
    ax.set(xlabel='x-position [mm]', ylabel='[a.u.]')
    x_pos = 1e3 * x_pos  # [m] -> [mm]
    colors = iter(['tab:blue', 'tab:orange'])
    if len(ref) > 1:
        ax.fill_between(x_pos, ref.min(axis=0), ref.max(axis=0), color=next(colors), alpha=0.5, label='Reference')
    else:
        ax.plot(x_pos, ref[0], color=next(colors), label='Reference')
    ax.plot(x_pos, out, color=next(colors), label='Data')
    ax.legend()
    return fig, ax


if __name__ == '__main__':
    args = parser.parse_args()

    if args.bin_edges:
        bin_edges = np.load(args.bin_edges)
    elif args.bins and args.edges:
        bin_edges = np.linspace(*args.edges, args.bins)
    else:
        raise RuntimeError('Either --bin-edges or --bins and --edges must be specified')
    x_pos = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    if args.output.suffix == '.csv':
        output = read_csv_output_file(args.output, only_detected=args.only_detected)
    elif args.output.suffix == '.xml':
        output = run_simulation(args.output, stdout=args.stdout, only_detected=args.only_detected)
        if args.save:
            output.to_csv(args.save)
    else:
        raise RuntimeError('Test data must be specified either as .csv file or .xml configuration file')

    if args.reference.suffix == '.csv':
        reference = read_csv_output_file(args.reference, only_detected=args.only_detected)
    elif args.reference.suffix == '.npy':
        reference = np.load(args.reference)
    else:
        raise RuntimeError('Reference data must be either .csv or .npy file')

    for stage_index, stage in enumerate(['initial', 'final']):
        output_histogram = np.histogram(output[f'{stage} x'], bins=bin_edges)[0]
        if isinstance(reference, pd.DataFrame):
            reference_profiles = np.histogram(reference[f'{stage} x'], bins=bin_edges)[0][np.newaxis, :]
        else:
            reference_profiles = reference[:, stage_index, :]

        fig, ax = plot_profiles(output_histogram, reference_profiles, x_pos=x_pos)
        ax.set_title(f'Comparing\n{args.output!s}\nand\n{args.reference!s}\n({stage})')
        if args.plot_grid:
            ax.grid()
        if args.log_scale:
            ax.set_yscale('log')

    plt.show()
