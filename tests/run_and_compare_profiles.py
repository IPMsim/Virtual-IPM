from argparse import ArgumentParser
from pathlib import Path
from typing import Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from virtual_ipm.auxiliaries import read_csv_output_file, run_simulation


parser = ArgumentParser()
config_help_text = (
    'File path to XML configuration or already existing CSV output file. The simulation will only be run for XML files '
    'while CSV files are loaded directly.'
)
parser.add_argument('configuration', type=Path, help=config_help_text)
parser.add_argument('reference', type=Path, help=config_help_text)
parser.add_argument('--resources', nargs='*', type=Path, help='Any files that the specified configuration needs to access')
parser.add_argument('--stdout', action='store_true', help='Show the stdout of the simulation')
parser.add_argument('--save', type=Path, help='Specify the directory where to save the output from the simulation. '
                                              'The file name will be the configuration name with ".csv" suffix.')
parser.add_argument('--bins', type=int, default=50, help='Number of bins used for the histogram when plotting profiles')


def compare_profiles(out: pd.DataFrame, ref: pd.DataFrame, *, title: str, bins: int, col: str):
    fig, ax = plt.subplots()
    ax.set(xlabel='x-position [mm]', ylabel='[a.u.]', title=title)
    x_edges = np.linspace(
        min(out[col].min(), ref[col].min()),
        max(out[col].max(), ref[col].max()),
        bins + 1,
    )
    x_pos = 0.5 * (x_edges[:-1] + x_edges[1:]) * 1e3  # [m] -> [mm]
    histograms = []
    for df in (ref, out):
        hist, __ = np.histogram(df[col], bins=x_edges)
        hist = hist.astype(float) / hist.sum()
        histograms.append(hist)
    for hist, label in zip(histograms, ('Reference', 'Configuration')):
        ax.plot(x_pos, hist, label=label)
    ax.legend()
    return fig, ax


def get_data(path: Path, *, save: Union[Path, None], stdout: bool, resources: list[Path]) -> pd.DataFrame:
    if path.suffix.lower() == '.xml':
        data = run_simulation(path, stdout=stdout, only_detected=True, resources=resources)
        if save:
            data.to_csv(save.joinpath(path.name).with_suffix('.csv'))
    elif path.suffix.lower() == '.csv':
        data = read_csv_output_file(path, only_detected=True)
    else:
        raise RuntimeError(f'Configuration file path must point to .XML or .CSV file (got {path.resolve()!s})')
    return data


if __name__ == '__main__':
    args = parser.parse_args()
    if args.save and not args.save.is_dir():
        raise RuntimeError('--save must point to an existing directory')
    if args.resources is None:
        args.resources = []
    output, reference = [get_data(path, save=args.save, stdout=args.stdout, resources=args.resources)
                         for path in (args.configuration, args.reference)]
    for stage in ('initial', 'final'):
        compare_profiles(
            output,
            reference,
            bins=args.bins,
            title=f'Comparing\n{args.configuration.resolve()!s}\nand\n{args.reference.resolve()!s}\n({stage} profiles)',
            col=f'{stage} x',
        )
    plt.show()
