# Virtual-IPM: Tests

The following text describes the various test scripts for the Virtual-IPM program.


## Running tests

Running the basic tests is as simple as invoking `pytest` inside the top-level directory `tests`
(you might need to install [pytest](https://pytest.org) first).


## Test cases

In the directory `cases` there are various XML configuration files which contain different combinations
of the available models. The purpose of these scripts is to run error-free without checking the output
for  correctness (in fact, they don't produce any output). They can be run via `cases/run_all.sh` or
simply by invoking `pytest` inside the top-level directory `tests`.


## End-to-end tests

These tests check the correctness of the output, produced by running specific XML configuration files,
and comparing it against some reference data. These tests are meant to allow for statistical fluctuations
and thus assess the correctness of the physics implementation, rather than a precise algorithmic procedure.

The available test cases are `{lhc,linac}.config.xml`. The files `*.output_ref.csv` are
example output files that have been obtained by running the corresponding configuration files.
These reference output CSV files can be used for (non-exact) comparison with newly obtained outputs.
The files `*.profiles.npy` contain reference profiles which have been obtained by running multiple
similar instances of the corresponding configuration files (with different RNG seeds). These files
contain a single Numpy array of shape `(N, 2, B)` where `N` is the number of instances run
(5000 for example), `2` corresponds to initial (index `0`) and final (index `1`) profiles and
`B` indicates the number of bins that have been used to produce these profiles
(100 for the `lhc` case and 160 for the `linac` case). Together with the `*.bin_edges.npy` files,
these can be used to compare newly produced output on a histogram basis. The `*.bin_edges.npy` files
contain a single Numpy array of shape `(B+1,)` and indicate the histogram bin edges have been used
to produce the profiles via `np.histogram`.

### Statistics considerations

The standard procedure for verifying that a newly generated profile conforms to the reference data
is to compute the min. and max. profile boundaries from all reference profiles and then asserting that
the newly generated profile lies within these boundaries; i.e.:

```py
# N: number of reference profiles
# 2: initial (i=0) and final (i=1) profile
# B: number of bins used to generated profiles
assert profiles.shape == (N, 2, B)
assert new_profile.shape == (2, B)
ref_min = profiles.min(axis=0)
ref_max = profiles.max(axis=0)
assert np.logical_and(new_profile >= ref_min, new_profile <= ref_max).all()
```

It should be noted that for a single profile (i.e. initial or final), the probability to lie outside
the reference boundaries is approximately `1/N` *for each bin separately* (where `N` is the number of
reference profiles). That is, the probability to lie *anywhere* (i.e. at *any bin*) outside the reference
boundaries is `1` minus the probability to lie everywhere *inside* the boundaries:

```py
prob_to_fail_anywhere = 1 - (1 - 1/N)**B
```

where `B` is the number of bins. For the standard configuration, i.e. 5000 profiles and `B = 100` for the
`lhc` case and `B = 160` for the `linac` case, this yields the following probabilities:

```py
>>> 1 - (1 - 1/5000)**100  # lhc
0.019803287350053767
>>> 1 - (1 - 1/5000)**160  # linac
0.031496517550193004
```

which is larger than 1% in both cases. For that reason, for the statistical comparison in `test_end_to_end.py`,
the profiles will be rebinned to have a smaller number of bins in order to decrease the probability of failure.
The number of effective bins that is used is `B = 50` for the `lhc` case and `B = 40` for the `linac` case:

```py
>>> 1 - (1 - 1/5000)**50  # lhc
0.009951156432195907
>>> 1 - (1 - 1/5000)**40  # linac
0.007968878893985454
```

### Environment used to generate the reference data

The environment and meta data describing under what conditions the reference data has been generated is contained
in `test_end_to_end_env_and_meta_data.json`. This JSON file contains as keys either shell commands (prefixed with `$ `;
e.g. `"$ python --version"`) or "normal" strings (no prefix; e.g. `"glibc-version"`). The values are either the raw
output of the corresponding shell command or any other value for the "normal" keys.
The RNG seeds used to generate the reference profiles were the numbers from `0` to `N-1`;
i.e. `rng_seeds = range(5000)` since 5000 reference profiles have been created for each test case.


## Helper scripts / functions

The following scripts are useful to further compare / inspect Virtual-IPM output:

* `compare_profiles.py` -- This script allows to compare the output of a simulation run (in CSV format)
  to reference data, either another CSV file or a collection of profiles in NPY format.
* `run_and_compare_profiles.py` -- This script accepts a configuration file and reference output file
  and runs the simulation in order to compare the thus produced output (it can also compare two outputs
  directly and as well run two configurations files and compare the output)
  

## Configurations

The `configurations` folder contains various configuration files which can be used to produce output
for different simulation models, in order to compare it to reference data.

* The `lhc_zspread*.xml` configurations can be used to test the `ZSpread*` particle generation models
  with different momentum contributions for the LHC case.
* The `test_GaussianDC_DCBeam.xml` and `test_GaussianDC_long_bunch.xml` configuration files can be used to test
  the DC bunch shape / electric field model with Gaussian transverse charge distribution 
  (see issue #269). A long single bunch beam emulates a DC beam by increasing the longitudinal standard 
  deviation (`sigma_z`) by three orders (`sigma_z*1e3`) and determine the bunch population accordingly by 
  comparing their linear density, `I/(beta*c) = N*e/(sqrt(2*pi)*sigma_z*1e3*beta*c)`. 
  Where `I` is the current of the DC beam, `c` is the speed of light in vacuum, `beta` is `v/c`, 
  `N` is the bunch population and `sigma_z` is the bunch length in the lab frame in dimension of time.
  Then verify that the final beam profiles produced by those two configurations match.
  They can be run and compared by running the `run_and_compare_profiles.py` script.  
* The `lhc_obstacles.xml` configuration can be used to test the `Obstacles` device model by comparing the
  output to `lhc.output_ref.csv` or `lhc.profiles.npy`. It needs to be run from within the `configurations`
  directory in order to access the required `configurations/resources/lhc_detector.stl` correctly.
  The following command can be used to run the test configuration (from within `configurations`):
  `python ../run_and_compare_profiles.py lhc_obstacles.xml ../lhc.output_ref.csv --resources resources/lhc_detector.stl`


## Plotting

Sometimes it is useful to assess the output of a specific simulation component by plotting it rather
than comparing the numbers. Various scripts for this purpose are available in the `plot` sub-folder,
sorted by component type (e.g. `particle_generation`). Please have a look at these scripts directly.


## GUI testing

For testing the GUI, the best way is to launch it (`virtual-ipm-gui`) and play around / use it, in order to
see if everything works. However, filling out entire configurations in the GUI can be tedious and for that
reason there exists a dedicated GUI testing script at `gui/components.py` which allows to launch single
tabs from the main GUI and only requires to fill those tabs in order to produce the corresponding XML or JSON
output text.

