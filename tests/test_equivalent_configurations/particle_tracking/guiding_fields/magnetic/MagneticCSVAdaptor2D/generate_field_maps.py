from pathlib import Path

import numpy as np
import pandas as pd


B = 0.2  # [T]

xs = 1e-3 * np.linspace(-43, 43, 9)  # [m]
ys = 1e-3 * np.linspace(-43, 43, 7)  # [m]
zs = 1e-3 * np.linspace(-90, 90, 5)  # [m]

x_grid, y_grid, z_grid = np.meshgrid(xs, ys, zs, indexing='ij')

Bx_grid = np.full_like(x_grid, 1e-3*B)
By_grid = np.full_like(x_grid, B)

Bx_grid *= (
      (1 - np.abs(np.linspace(-0.05, 0.05, xs.size)))[:,None,None]
    * (1 - np.abs(np.linspace(-0.06, 0.06, ys.size)))[None,:,None]
    * (1 - np.abs(np.linspace(-0.07, 0.07, zs.size)))[None,None,:]
)
By_grid *= (
      (1 - np.abs(np.linspace(-0.08, 0.08, xs.size)))[:,None,None]
    * (1 - np.abs(np.linspace(-0.09, 0.09, ys.size)))[None,:,None]
    * (1 - np.abs(np.linspace(-0.10, 0.10, zs.size)))[None,None,:]
)

df = pd.DataFrame(
    data=np.stack(
        [
            x_grid.ravel(),
            y_grid.ravel(),
            z_grid.ravel(),
            Bx_grid.ravel(),
            By_grid.ravel(),
        ],
        axis=1,
    ),
    columns=[
        'x [m]',
        'y [m]',
        'z [m]',
        'Bx [T]',
        'By [T]',
    ],
)

folder = Path(__file__).parent
df.to_csv(folder/'field_map_by_column_position.csv', index=False)
df[['x [m]', 'y [m]', 'z [m]', 'By [T]', 'Bx [T]']].to_csv(folder/'field_map_by_column_name.csv', index=False)
