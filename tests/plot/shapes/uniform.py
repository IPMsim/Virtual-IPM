from argparse import ArgumentParser
import matplotlib.pyplot as plt
import numpy as np
from virtual_ipm.simulation.beams.bunches.shapes import Uniform


parser = ArgumentParser()
parser.add_argument('radius', type=float)
parser.add_argument('sz', type=float)

args = parser.parse_args()

Uniform._radius = args.radius
Uniform._longitudinal_sigma = args.sz

shape = Uniform(None)


def test_length(shape):
    assert shape.length == 8 * args.sz


def test_normalized_density_at(shape):
    N = 100
    z_1 = np.full(N**2, -2 * args.sz)
    z_2 = np.zeros(N**2, dtype=float)
    z_3 = np.full(N**2, 2 * args.sz)

    y, x = np.meshgrid(*[np.linspace(-1.5*args.radius, 1.5*args.radius, N)]*2)

    def plot_density(z):
        density = shape.normalized_density_at(np.stack([x.ravel(), y.ravel(), z]))
        plt.title('z = {} (max.: {:e})'.format(z, density.max()))
        plt.xlabel('x [m]')
        plt.ylabel('y [m]')
        plt.scatter(x.ravel(), y.ravel(), c=density)
        plt.colorbar()
        plt.grid()
        plt.xlim([-1.5*args.radius, 1.5*args.radius])
        plt.ylim([-1.5*args.radius, 1.5*args.radius])

    plt.subplots(2, 2)

    plt.subplot(2, 2, 1)
    plot_density(z_1)

    plt.subplot(2, 2, 2)
    plot_density(z_2)

    plt.subplot(2, 2, 3)
    plot_density(z_3)


def test_normalized_linear_density_at(shape):
    z = np.linspace(-5 * args.sz, 5 * args.sz, 1000)
    density = shape.normalized_linear_density_at(z)
    plt.title('Linear density (integral: {})'.format(np.sum(density * (z[1] - z[0]))))
    plt.xlabel('z [m]')
    plt.ylabel('density 1/m^3')
    plt.plot(z, density)
    plt.grid()


def test_generate_positions_in_transverse_plane(shape):
    x, y = shape.generate_positions_in_transverse_plane(50000, 0)
    plt.title('Generated positions')
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.scatter(x, y, s=1)
    plt.grid()
    plt.xlim([-1.5 * args.radius, 1.5 * args.radius])
    plt.ylim([-1.5 * args.radius, 1.5 * args.radius])


test_length(shape)
test_normalized_density_at(shape)
test_normalized_linear_density_at(shape)
test_generate_positions_in_transverse_plane(shape)

plt.show()
