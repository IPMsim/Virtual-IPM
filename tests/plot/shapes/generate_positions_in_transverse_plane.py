#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2021  The IPMSim collaboration <https://ipmsim.gitlab.io/>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser

from anna import XMLAdaptor
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy

import virtual_ipm.simulation.beams.bunches.shapes as bunch_shapes


parser = ArgumentParser()
parser.add_argument('shape', help='Name of bunch shape model')
parser.add_argument('config', help='Path to XML configuration file')
args = parser.parse_args()

try:
    bunch_shape_cls = getattr(bunch_shapes, args.shape)
except AttributeError:
    raise ValueError('Unknown bunch shape: %s' % args.shape)

config = XMLAdaptor(args.config)
n_samples = int(config.get_text('NSamples'))
z_position = float(config.get_text('ZPosition'))

bunch_shape = bunch_shape_cls(config)
transverse_positions = bunch_shape.generate_positions_in_transverse_plane(n_samples, z_position)
transverse_positions *= 1000.

figure = plt.figure()
axes = figure.add_subplot(111)
axes.scatter(transverse_positions[0], transverse_positions[1], marker='.')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')


x_min = numpy.min(transverse_positions[0])
x_max = numpy.max(transverse_positions[0])

# Allow deviation of 3mm from y=0.
y0_positions = transverse_positions[:, numpy.abs(transverse_positions[1]) <= 3.]

# Bin size 2mm.
bins, edges = numpy.histogram(y0_positions[0], bins=int(x_max - x_min)//2)
edges = edges[:-1]

figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(edges, bins)
axes.set_xlabel('x [mm]')
axes.set_ylabel('Number of generated particles')
axes.set_title('|y| <= 3')


y_min = numpy.min(transverse_positions[1])
y_max = numpy.max(transverse_positions[1])

# Allow deviation of 3mm from x=0.
x0_positions = transverse_positions[:, numpy.abs(transverse_positions[0]) <= 3.]

# Bin size 2mm.
bins, edges = numpy.histogram(x0_positions[1], bins=int(y_max - y_min)//2)
edges = edges[:-1]

figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(edges, bins)
axes.set_xlabel('y [mm]')
axes.set_ylabel('Number of generated particles')
axes.set_title('|x| <= 3')

plt.show()
