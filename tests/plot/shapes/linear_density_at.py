import argparse

import anna
import matplotlib.pyplot as plt
import numpy
import virtual_ipm.simulation.beams.bunches.shapes as bunch_shapes


configurations = {
    'Gaussian': anna.JSONAdaptor(root={
        'BunchShape/Parameters/TransverseSigma': {
            'text': '[ 5e-3, 10e-3 ]',
            'meta': {'unit': 'm'}
        },
        'BunchShape/Parameters/LongitudinalSigmaBunchFrame': {
            'text': '0.25',
            'meta': {'unit': 'm'}
        }
    }),
    'ParabolicEllipsoid': anna.JSONAdaptor(root={
        'BunchShape/Parameters/SemiMajorAxis': {
            'text': '1',
            'meta': {'unit': 'm'}
        },
        'BunchShape/Parameters/SemiMinorAxis': {
            'text': '5e-3',
            'meta': {'unit': 'm'}
        }
    }),
    'HollowDCBeam': anna.JSONAdaptor(root={
        'Parameters/Energy': {
            'text': '1',
            'meta': {'unit': 'GeV'}
        },
        'Parameters/ParticleType/ChargeNumber': '1',
        'Parameters/ParticleType/RestEnergy': {
            'text': '%(proton mass energy equivalent in MeV)',
            'meta': {'unit': 'MeV'}
        },
        'BunchShape/Parameters/InnerRadius': {
            'text': '5e-3',
            'meta': {'unit': 'm'}
        },
        'BunchShape/Parameters/OuterRadius': {
            'text': '10e-3',
            'meta': {'unit': 'm'}
        },
        'BunchShape/Parameters/BeamCurrent': {
            'text': '1',
            'meta': {'unit': 'A'}
        }
    })
}

parser = argparse.ArgumentParser()
parser.add_argument('model', choices=tuple(configurations))


if __name__ == '__main__':
    args = parser.parse_args()

    bunch_shape_model = getattr(bunch_shapes, args.model)
    bunch_shape = bunch_shape_model(configurations[bunch_shape_model.__name__])

    z_positions = numpy.linspace(-1.2, 1.2, 1000)

    density = bunch_shape.normalized_linear_density_at(z_positions)

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.plot(z_positions, density)
    axes.set_xlabel('z [m]')
    axes.set_ylabel('Normalized linear particle density')
    axes.set_title('')

    plt.show()
