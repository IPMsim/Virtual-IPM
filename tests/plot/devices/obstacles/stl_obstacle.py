from argparse import ArgumentParser
import itertools as it
from pathlib import Path
from tempfile import TemporaryDirectory

import matplotlib.pyplot as plt
import numpy as np

from virtual_ipm.auxiliaries import run_simulation
from virtual_ipm.simulation.devices.models import Obstacles
from virtual_ipm.simulation.devices.obstacles import STLObstacle


parser = ArgumentParser()
parser.add_argument('file')
parser.add_argument('--box-center', nargs=3, type=float, required=True, help='[m]')
parser.add_argument('--box-edge-length', nargs=3, type=float, required=True, help='[m]')
parser.add_argument('--n-particles', default=10**4, type=int)
parser.add_argument('--color-detected', default='tab:orange')
parser.add_argument('--color-tracked', default='tab:blue')
parser.add_argument('--scatter-size', default=3, type=float)
parser.add_argument('--stdout', action='store_true', help='Show stdout of the simulation')
parser.add_argument('--color-alpha-3d', default=0.25, type=float, help='Alpha value for 3D scatter plot')


if __name__ == '__main__':
    args = parser.parse_args()

    bounding_box = Obstacles.create_bounding_box_from_stl_obstacle(STLObstacle(args.file))
    print(
        f'The bounding box is:'
        f'\n\tcenter = {bounding_box.center.ravel().tolist()}'
        f'\n\tedge length = {(bounding_box.half_size*2).ravel().tolist()}'
        ,
        end='\n\n',
    )

    template = Path('stl.template.xml').read_text()
    config = template.format(
        stl_filepath=str(Path(args.file).resolve()),
        box_center_in_m=list(args.box_center),
        box_edge_length_in_m=list(args.box_edge_length),
        n_particles=args.n_particles,
    )
    with TemporaryDirectory() as td:
        td = Path(td)
        config_path = td / 'config.xml'
        config_path.write_text(config)
        result = run_simulation(config_path, stdout=args.stdout, only_detected=False)
        detected = result[result['status'] == 'DETECTED']
        tracked = result[result['status'] == 'TRACKED']
        assert len(detected) + len(tracked) == len(result)

    fig3d = plt.figure()
    ax3d = fig3d.add_subplot(projection='3d')
    ax3d.set(xlabel='x [mm]', ylabel='y [mm]', zlabel='z [mm]')
    ax3d.scatter(
        tracked['final x'],
        tracked['final y'],
        tracked['final z'],
        color=args.color_tracked,
        s=args.scatter_size,
        alpha=args.color_alpha_3d,
    )
    ax3d.scatter(
        detected['final x'],
        detected['final y'],
        detected['final z'],
        color=args.color_detected,
        s=args.scatter_size,
        alpha=args.color_alpha_3d,
    )

    fig, axes = plt.subplots(ncols=3, figsize=(16, 5))
    for i, (a, b) in enumerate(it.combinations('xyz', 2)):
        axes[i].set(xlabel=f'{a} [mm]', ylabel=f'{b} [mm]')
        axes[i].scatter(
            tracked[f'final {a}'],
            tracked[f'final {b}'],
            color=args.color_tracked,
            s=args.scatter_size,
        )
        axes[i].scatter(
            detected[f'final {a}'],
            detected[f'final {b}'],
            color=args.color_detected,
            s=args.scatter_size,
        )

    plt.show()
