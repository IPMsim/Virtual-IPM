from argparse import ArgumentParser

import matplotlib.pyplot as plt
import numpy as np

from virtual_ipm.simulation.devices.obstacles.obstacle import Box, MultilayerObstacle


parser = ArgumentParser()
parser.add_argument('--center', nargs=3, type=float, default=(5, 5, 0))
parser.add_argument('--size', nargs=3, type=float, default=(4, 2, 1))
parser.add_argument('--n-boxes', type=int, default=3, help='Each box uses half the outer box\' size')
parser.add_argument('--n', type=int, default=10**4, help='Number of particles')
parser.add_argument('--xy-boundaries', nargs=2, type=float, default=(0, 10),
                    help='Boundaries for random number generation')
parser.add_argument('--z', type=float, default=0, help='z-coordinate of particles')
parser.add_argument('--s', type=float, default=3, help='Point size for scatter plot')


if __name__ == '__main__':
    args = parser.parse_args()

    boxes = []
    for i in range(args.n_boxes):
        scaling = 0.5 ** i
        boxes.append(Box(center=args.center, size=[scaling*x for x in args.size]))
    obstacle = MultilayerObstacle(boxes)

    positions = np.zeros((3, args.n), dtype=float)
    positions[:2] = np.random.default_rng().uniform(*args.xy_boundaries, size=(2, args.n))
    positions[2] = np.full_like(positions[0], args.z)

    fig, ax = plt.subplots()
    ax.set(xlabel='x', ylabel='y')
    p = ax.scatter(positions[0], positions[1], c=obstacle.collide(positions), s=args.s)
    fig.colorbar(p)

    plt.show()
