"""Script to transform (translate and scale) STL files using the trimesh package."""

from argparse import ArgumentParser

import numpy as np
import trimesh


parser = ArgumentParser()
parser.add_argument('file')
parser.add_argument('out')
parser.add_argument('--scale-x', default=1.0, type=float, help='Scaling factor for x-dimension')
parser.add_argument('--scale-y', default=1.0, type=float, help='Scaling factor for y-dimension')
parser.add_argument('--scale-z', default=1.0, type=float, help='Scaling factor for z-dimension')
parser.add_argument('--scale', nargs=3, type=float, help='Allows to specify all three scaling factors at once '
                                                         '(takes precedence over the individual ones)')
parser.add_argument('--translate-x', default=0.0, type=float, help='Translation in x-dimension')
parser.add_argument('--translate-y', default=0.0, type=float, help='Translation in y-dimension')
parser.add_argument('--translate-z', default=0.0, type=float, help='Translation in z-dimension')
parser.add_argument('--translate', nargs=3, type=float, help='Allows to specify all three translations at once '
                                                             '(takes precedence over the individual ones)')
parser.add_argument('--plot', action='store_true', help='Plot the transformed mesh')
parser.add_argument('--plot-origin-size', default=0.001, type=float, help='Display size of the origin')


if __name__ == '__main__':
    args = parser.parse_args()
    if args.scale is not None:
        args.scale_x, args.scale_y, args.scale_z = args.scale
    if args.translate is not None:
        args.translate_x, args.translate_y, args.translate_z = args.translate

    mesh = trimesh.load_mesh(file_obj=args.file, file_type='stl')

    transformation_matrix = np.array([
        [args.scale_x, 0           , 0           , args.translate_x],
        [0           , args.scale_y, 0           , args.translate_y],
        [0           , 0           , args.scale_z, args.translate_z],
        [0           , 0           , 0           , 1               ],
    ])
    mesh = mesh.apply_transform(transformation_matrix)

    trimesh.exchange.export.export_mesh(mesh=mesh, file_obj=args.out, file_type='stl')

    box_corners = trimesh.bounds.corners(mesh.bounding_box.bounds)
    box_center = box_corners.mean(axis=0)
    box_edge_length = mesh.bounding_box.primitive.extents
    print(
        f'Bounding box:'
        f'\n\tcorners: {box_corners.tolist()}'
        f'\n\tcenter: {box_center.tolist()}'
        f'\n\tedge length: {box_edge_length.tolist()}'
    )

    if args.plot:
        axis_xyz = trimesh.creation.axis(origin_size=args.plot_origin_size)
        scene = trimesh.Scene([axis_xyz, mesh])
        scene.show()
