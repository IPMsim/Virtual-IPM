import argparse
from argparse import Namespace
import inspect

import anna
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy
import scipy.constants as constants
import scipy.stats
from virtual_ipm.simulation.particle_generation.models import GasJet


configuration = anna.JSONAdaptor(root={
    'ParticleGeneration/Parameters/Velocity': {
        'text': '500',
        'meta': {'unit': 'm/s'}
    },
    'ParticleGeneration/Parameters/TransverseTemperature': {
        'text': '300',
        'meta': {'unit': 'K'}
    },
    'ParticleGeneration/Parameters/LongitudinalTemperature': {
        'text': '50',
        'meta': {'unit': 'K'}
    },
    # 14-Nitrogen.
    'ParticleGeneration/Parameters/Mass': {
        'text': '14.0030740052',
        'meta': {'unit': 'u'}
    },
    'ParticleGeneration/Parameters/ZPosition': {
        'text': '0',
        'meta': {'unit': 'm'}
    }
})

setup = Namespace(
    number_of_particles=50000,
    particle_type=Namespace(
        mass=14.0030740052 * constants.physical_constants['unified atomic mass unit'][0]
   )
)


class BeamMock:
    def __getitem__(self, item):
        return self


def compute_number_of_particles_to_be_created_mock(*args):
    return setup.number_of_particles

model = GasJet(BeamMock(), None, setup, configuration)
model.compute_number_of_particles_to_be_created = compute_number_of_particles_to_be_created_mock


def setup_figure_and_axes(figure, axes, xlabel, ylabel, title):
    # axes.grid(True, lw=2)
    axes.grid(True, lw=1, which='major')
    axes.grid(True, lw=0.25, which='minor')
    # axes.grid(True, lw=1, which='major')
    plt.minorticks_on()
    axes.margins(0.05)
    axes.title.set_position((.5, 1.02))
    axes.tick_params(axis='both', which='major', pad=5)
    axes.set_xlabel(xlabel, fontsize=36)
    axes.set_ylabel(ylabel, fontsize=36)
    axes.tick_params(axis='x', labelsize=36)
    axes.tick_params(axis='y', labelsize=36)
    axes.set_title(title, fontsize=40)
    axes.legend(shadow=False, fontsize=36, markerscale=10)
    # figure.tight_layout()
    for spine in axes.spines.values():
        spine.set_linewidth(2)


def test_1d():
    velocities = model.generate_momenta(None) / setup.particle_type.mass
    transverse_abs_vals = numpy.sqrt(velocities[0]**2 + velocities[2]**2)
    longitudinal_abs_vals = numpy.abs(velocities[1])

    hist_t = numpy.histogram(transverse_abs_vals, bins=500)
    hist_l = numpy.histogram(longitudinal_abs_vals, bins=500)

    # Maxwell-Boltzmann distribution for |\vec{v}| in two dimensions.
    def maxwell_boltzmann(x):
        return 1. / model._transverse_scale**2 * x * numpy.exp(
            -x**2 / (2 * model._transverse_scale**2)
        )

    mb_xs_t = hist_t[1][:-1] + (hist_t[1][1] - hist_t[1][0]) / 2.
    mb_ys_t = maxwell_boltzmann(mb_xs_t)
    mb_ys_t *= numpy.sum(hist_t[0]) / numpy.sum(mb_ys_t)

    # Maxwell-Boltzmann distribution for |\vec{v}| in one dimension.
    def normal_distribution(x):
        return 1. / numpy.sqrt(2 * numpy.pi * model._longitudinal_scale**2) * numpy.exp(
            -x**2 / (2 * model._longitudinal_scale**2)
        )

    mb_xs_l = hist_l[1][:-1] + (hist_l[1][1] - hist_l[1][0]) / 2.
    mb_ys_l = normal_distribution(mb_xs_l - model._velocity)
    mb_ys_l *= numpy.sum(hist_l[0]) / numpy.sum(mb_ys_l)

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.plot(hist_t[1][:-1], hist_t[0], label=r'Sampled $\perp$', lw=2)
    axes.plot(mb_xs_t, mb_ys_t, '--', label=r'Maxwell-Boltzmann $\perp$', lw=2)
    axes.plot(hist_l[1][:-1], hist_l[0], label=r'Sampled $||$', lw=2)
    axes.plot(mb_xs_l, mb_ys_l, '--', label=r'Maxwell-Boltzmann $||$', lw=2)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm |\vec{v}^{\perp}|\,, |\vec{v}^{||}|\; [m/s]$',
        '[a.u.]',
        'Transverse and longitudinal velocity distribution'
    )

    plt.show()


def test_1d_proj():
    velocities = model.generate_momenta(None) / setup.particle_type.mass

    hist_x = numpy.histogram(velocities[0], bins=500)
    hist_y = numpy.histogram(velocities[1], bins=500)
    hist_z = numpy.histogram(velocities[2], bins=500)

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.plot(hist_x[1][:-1], hist_x[0], label=r'$\rm v_x$', lw=2)
    axes.plot(hist_y[1][:-1], hist_y[0], label=r'$\rm v_y$', lw=2)
    axes.plot(hist_z[1][:-1], hist_z[0], label=r'$\rm v_z$', lw=2)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_i\; [m/s]$',
        '[a.u.]',
        'Velocity distributions; 1d-projections'
    )

    plt.show()


def test_2d():
    velocities = model.generate_momenta(None) / setup.particle_type.mass

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.scatter(velocities[0], velocities[1], s=1)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_x\; [m/s]$',
        r'$\rm v_y\; [m/s]$',
        'xy-velocity distribution'
    )

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.scatter(velocities[0], velocities[2], s=1)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_x\; [m/s]$',
        r'$\rm v_z\; [m/s]$',
        'xz-velocity distribution'
    )

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.scatter(velocities[1], velocities[2], s=1)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_y\; [m/s]$',
        r'$\rm v_z\; [m/s]$',
        'yz-velocity distribution'
    )

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.scatter(velocities[0], velocities[1], label=r'$\rm v_x\,v_y$', s=1)
    axes.scatter(velocities[0], velocities[2], label=r'$\rm v_x\,v_z$', s=1)
    axes.scatter(velocities[1], velocities[2], label=r'$\rm v_y\,v_z$', s=1)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_i\; [m/s]$',
        r'$\rm v_j\; [m/s]$',
        '2d-velocity distributions'
    )

    plt.show()


def test_3d():
    velocities = model.generate_momenta(None) / setup.particle_type.mass

    figure = plt.figure()
    # axes = figure.gca(projection='3d')
    axes = figure.add_subplot(111, projection='3d')
    # axes = figure.add_subplot(111)
    axes.scatter(velocities[0], velocities[1], velocities[2], s=1)
    setup_figure_and_axes(
        figure,
        axes,
        r'$\rm v_x\; [m/s]$',
        r'$\rm v_y\; [m/s]$',
        'Velocity distribution'
    )
    axes.set_zlabel(r'$\rm v_z\; [m/s]$', fontsize=36)
    axes.tick_params(axis='z', labelsize=36)

    plt.show()


TEST_FUNCS = {name.removeprefix('test_'): func
              for name, func in globals().items()
              if inspect.isfunction(func) and name.startswith('test_')}

parser = argparse.ArgumentParser()
parser.add_argument('dim', choices=tuple(TEST_FUNCS))


if __name__ == '__main__':
    args = parser.parse_args()
    TEST_FUNCS[args.dim]()
