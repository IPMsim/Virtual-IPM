import tempfile
from argparse import ArgumentParser
from pathlib import Path
import re

import matplotlib.pyplot as plt

from virtual_ipm.auxiliaries import read_csv_output_file, run_simulation


parser = ArgumentParser()
parser.add_argument('configuration', type=Path, help='File path to XML configuration'
                                                     'The XML file should be ParticleGenerationOnly ...')
parser.add_argument('--stdout', action='store_true', help='Show the stdout of the simulation')
parser.add_argument('--save', type=Path, help='Specify this file path to save the output from the simulation')
parser.add_argument('--n-particles', type=int, help='Specify a different number of particles to be used')
parser.add_argument('--n-time-steps', type=int, help='Specify a different number of time steps to be used '
                                                     '(the configuration file must already specify a '
                                                     '<NumberOfTimeSteps> parameter for this to work)')
parser.add_argument('--waterfall', action='store_true', help='Plot the t-coordinates on the y-axis instead of x-axis')


def plot_hist(t_steps, z_values, *, waterfall):
    fig, axs = plt.subplots(3, 1, constrained_layout=True)
    axs[0].set_title('2D Histogram over time steps and z values')
    if waterfall:
        axs[0].hist2d(z_values, t_steps, bins=50)
        axs[0].set_xlabel('distance (m)')
        axs[0].set_ylabel('time step')
    else:
        axs[0].hist2d(t_steps, z_values, bins=50)
        axs[0].set_xlabel('time step')
        axs[0].set_ylabel('distance (m)')
    fig.suptitle('Distribution of the generated particles', fontsize=16)
    axs[1].hist(t_steps, bins=50)
    axs[1].set_xlabel('time step')
    axs[1].set_title('Histogram over time steps, p(t)')
    axs[1].set_ylabel('number')
    axs[2].hist(z_values, bins=50)
    axs[2].set_xlabel('time (s)')
    axs[2].set_title('Histogram over z values, p(z)')
    axs[2].set_ylabel('number')
    return fig, axs


if __name__ == '__main__':
    args = parser.parse_args()

    if args.configuration.suffix.lower() == '.xml':
        config_text = args.configuration.read_text()
        if '<OnlyGenerateParticles>' not in config_text:
            config_text = config_text.replace('<Simulation>',
                                              '<Simulation><OnlyGenerateParticles>true</OnlyGenerateParticles>')
        if args.n_particles:
            config_text = re.sub(r'(?<=>)\s*[0-9]+\s*(?=</NumberOfParticles>)', f'{args.n_particles}', config_text)
        if args.n_time_steps:
            if '</NumberOfTimeSteps>' not in config_text:
                raise RuntimeError('The configuration must specify a "Simulation/NumberOfTimeSteps" parameter '
                                   'in order to customize this number via --n-time-steps')
            config_text = re.sub(r'(?<=>)\s*[0-9]+\s*(?=</NumberOfTimeSteps>)', f'{args.n_time_steps}', config_text)
        with tempfile.TemporaryDirectory() as td:
            config_path = Path(td) / 'config.xml'
            config_path.write_text(config_text)
            output = run_simulation(config_path, stdout=args.stdout)
        if args.save:
            output.to_csv(args.save)
    elif args.configuration.suffix.lower() == '.csv':
        output = read_csv_output_file(args.configuration)
    else:
        raise RuntimeError('Configuration file path must point to .XML or .CSV file')

    plot_hist(output['initial sim. step'], output['initial z'], waterfall=args.waterfall)
    plt.show()
