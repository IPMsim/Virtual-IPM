# Resources used by configurations

* `lhc_detector.stl` Box with the following properties (in millimeters):
  * Center: `[0, -43, 0]`
  * Edge length: `[20, 1, 200]`

  This box is meant to resemble the `BasicIPM` properties from `tests/lhc.config.xml`.
  Hence, it allows to compare the two different device models `Obstacles` and `BasicIPM` for that use case.
