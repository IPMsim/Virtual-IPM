import os
from pathlib import Path
import tempfile

import pytest


@pytest.fixture()
def read_template():
    def reader(f_path):
        template = Path(f_path).read_text()
        template = template.replace('/dev/null', os.devnull)
        template = template.replace('/tmp/', f'{tempfile.gettempdir()}{os.path.sep}')
        return template
    return reader
