import numpy as np

rng = np.random.default_rng(seed=0)

SCALE = 2.5
LIMITS = (-4*SCALE, 4*SCALE)
BINS = np.linspace(*LIMITS, 50)
N_PARTICLES = 250_000

histograms = []
for _ in range(10**4):
    bins, __ = np.histogram(rng.normal(scale=SCALE, size=N_PARTICLES), bins=BINS)
    histograms.append(bins)

histograms = np.stack(histograms)
reference = histograms[0]  # choose any
difference = np.abs(histograms - reference)

print(np.max(difference, axis=0))
print(np.max(difference / reference, axis=0))

max_abs_diff = int(1e-3 * N_PARTICLES)
mask = np.any(difference > max_abs_diff, axis=0)
print(f'Max. relative difference for {max_abs_diff=}: '
      f'{np.max((difference[:, mask] - max_abs_diff) / reference[mask])}')
