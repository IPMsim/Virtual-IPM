#!/bin/bash

devnull=$(python -c 'import os; print(os.devnull)')
tempdir=$(python -c 'import os; import tempfile; print(repr(f"{tempfile.gettempdir()}{os.path.sep}"))')
tempdir="${tempdir:1:-1}"

status=0
for f in *.xml
do
  sed "s!/dev/null!$devnull!g;s!/tmp/!$tempdir!g" "$f" > _tmp.xml
  virtual-ipm _tmp.xml > /dev/null 2>&1
  current_status=$?
  rm _tmp.xml
	status=$(( status | current_status ))
	echo "$f -- exit code: $current_status"
done
exit $(( status ))
