#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2021  The IPMSim collaboration <https://ipmsim.gitlab.io/>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser
import sys

from anna.frontends.qt.parameters import InvalidInputError
from PyQt5.QtWidgets import QWidget, QPushButton, QMessageBox, QVBoxLayout, QHBoxLayout, QApplication

from virtual_ipm.frontends.gui.views import Beams, Device, ParticleGeneration, ParticleTracking, \
    ElectricGuidingField, MagneticGuidingField, Simulation, Output


VIEWS = {v.__name__: v for v in [Beams, Device, ParticleGeneration, ParticleTracking, ElectricGuidingField,
                                 MagneticGuidingField, Simulation, Output]}

parser = ArgumentParser()
parser.add_argument('view', choices=tuple(VIEWS))


class TestWidget(QWidget):
    def __init__(self, widget, parent=None):
        super().__init__(parent)
        self._widget = widget
        self._json_button = QPushButton('JSON')
        self._xml_button = QPushButton('XML')

        def print_json():
            try:
                print(self._widget.dump_as_json())
            except InvalidInputError as err:
                QMessageBox.critical(self, err.__class__.__name__, str(err))

        def print_xml():
            try:
                print(self._widget.dump_as_xml())
            except InvalidInputError as err:
                QMessageBox.critical(self, err.__class__.__name__, str(err))

        self._json_button.clicked.connect(print_json)
        self._xml_button.clicked.connect(print_xml)

        v_layout = QVBoxLayout()
        v_layout.addWidget(widget)
        h_layout = QHBoxLayout()
        h_layout.addStretch(1)
        h_layout.addWidget(self._json_button)
        h_layout.addWidget(self._xml_button)
        v_layout.addLayout(h_layout)
        self.setLayout(v_layout)
        self.setWindowTitle(self._widget.__class__.__name__)


if __name__ == '__main__':
    args = parser.parse_args()

    app = QApplication(sys.argv)
    test_widget = TestWidget(VIEWS[args.view]())
    test_widget.show()

    sys.exit(app.exec_())
