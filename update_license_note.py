import itertools as it
from pathlib import Path


with open('virtual_ipm/__init__.py') as fh:
    LICENSE_NOTE = ''.join(it.takewhile(lambda line: line.startswith('#'), fh))

OLD = 'Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>'
NEW = 'Copyright (C) 2021  The IPMSim collaboration <https://ipmsim.gitlab.io/>'

if OLD not in LICENSE_NOTE:
    raise RuntimeError('The to-be-replaced string is not part of the license note')


if __name__ == '__main__':
    for file in Path('virtual_ipm').glob('**/*.py'):
        content = file.read_text()
        if LICENSE_NOTE not in content:
            print(f'[{file!s}] does not contain the license note')
            continue
        content = content.replace(OLD, NEW, 1)
        file.write_text(content)
